import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Suspense } from "react";
import "./App.css";
import Navbar from "./components/navbar/Navbar.tsx";
import Categories from "./components/categories/Categories.tsx";
import CategoryItem from "./components/categories/categoryItem/CategoryItem.tsx";
import Home from "./components/home/Home.jsx";
import NoMatch from "./components/404/NoMatch.jsx";
import Orders from "./components/orders/Orders.jsx";

import { QueryClient, QueryClientProvider, useQuery } from "react-query";
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Navbar />

      <Suspense fallback={<div className="container">Loading...</div>}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/categories" element={<Categories />} />
          <Route path="/categories/:slug" element={<CategoryItem />} />
          <Route path="*" element={<NoMatch />} />
        </Routes>
      </Suspense>
    </QueryClientProvider>
  );
}

export default App;
