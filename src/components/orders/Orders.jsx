import React from "react";
import { useQuery } from "react-query";
import axios from "axios";
import { useEffect, useState } from "react";

const ordersArray = [
  { orderNum: 1, title: "order1", status: "pending" },
  { orderNum: 2, title: "order2", status: "pending" },
  { orderNum: 3, title: "order3", status: "pending" },
];

const Orders = () => {
  useEffect(() => {
    // Creating a timeout within the useEffect hook
    const intervalId = setInterval(() => {
      ordersArray.map((order) => {
        if (order.status === "pending") {
          order.status = "inProgress";
        } else if (order.status === "inProgress") {
          order.status = "in-delivery";
        } else if (order.status === "in-delivery") {
          order.status = "delivered";
        }
      });
    }, 30000);
    return () => clearTimeout(intervalId);
  }, [ordersArray]);

  //// In a Real project the results and changes are being observed and we could use
  // invalidating query to
  // cause less redundency in requesting .
  const {
    isLoading,
    error,
    data: orders,
  } = useQuery(
    "orderTracking",
    async () => {
      const { data } = await axios.get(
        "https://random-data-api.com/api/vehicle/random_vehicle"
      );
      return data;
    },
    {
      refetchInterval: 5000,
      refetchIntervalInBackground: true,
    }
  );
  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;
  return (
    <div className="py-14 px-4 md:px-6 2xl:px-20 2xl:container 2xl:mx-auto">
      <div className="p-5 w-full h-full flex justify-around content-center">
        <p className="text-lg md:text-xl font-semibold leading-6 xl:leading-5 text-gray-800">
          Customer’s Orders
        </p>
        {!isLoading &&
          !error &&
          ordersArray.map((order, index) => (
            <div
              key={index}
              className="mt-10 flex flex-col xl:flex-row jusitfy-center items-stretch  w-full xl:space-x-8 space-y-4 md:space-y-6 xl:space-y-0"
            >
              <div className="flex flex-col justify-start items-start w-full space-y-4 md:space-y-6 xl:space-y-8">
                <div className="flex flex-col justify-start items-start bg-gray-50 px-4 py-4 md:py-6 md:p-6 xl:p-8 w-full">
                  <div className="mt-4 md:mt-6 flex  flex-col md:flex-row justify-start items-start md:items-center md:space-x-6 xl:space-x-8 w-full ">
                    <div className="pb-4 md:pb-8 w-full md:w-40">
                      <img
                        className="w-full hidden md:block "
                        src="https://images.pexels.com/photos/116675/pexels-photo-116675.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                        alt="Product"
                      />
                      <img className="w-full md:hidden" src="" alt="Product" />
                    </div>
                    <div className="border-b border-gray-200 md:flex-row flex-col flex justify-between items-start w-full  pb-8 space-y-4 md:space-y-0">
                      <div className="w-full flex flex-col justify-start items-start space-y-8">
                        <h3 className="text-xl xl:text-2xl font-semibold leading-6 text-gray-800">
                          {order.title}
                        </h3>
                        <div className="flex justify-start items-start flex-col space-y-2">
                          <p className="text-sm leading-none text-gray-800">
                            <span className="text-gray-300 font-bold">
                              STATUS:{" "}
                            </span>{" "}
                            {order.status}
                          </p>
                        </div>
                      </div>
                      <div className="flex justify-between space-x-8 items-start w-full">
                        <p className="text-base xl:text-lg leading-6">
                          {order.price}
                          <span className="text-red-300 line-through">
                            {" "}
                            {order.price}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Orders;
