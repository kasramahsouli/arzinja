import React from "react";
const CategoriesArray = [
  {
    id: 1,
    title: "BENZ",
    description: "some info about Benz ",
    image:
      "https://images.pexels.com/photos/163774/star-vehicle-pkw-mercedes-163774.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 2,
    title: "BMW",
    description: "some info about Benz ",
    image:
      "https://images.pexels.com/photos/170811/pexels-photo-170811.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 3,
    title: "TOYOTA",
    description: "some info about Benz ",
    image:
      "https://images.pexels.com/photos/4061413/pexels-photo-4061413.jpeg?auto=compress&cs=tinysrgb&w=600",
  },
  {
    id: 4,
    title: "TESLA",
    description: "some info about Benz ",
    image:
      "https://images.pexels.com/photos/11099566/pexels-photo-11099566.jpeg?auto=compress&cs=tinysrgb&w=600",
  },
];

const Categories = () => {
  return (
    <div>
      <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8 mt-10">
        {CategoriesArray.map((item: any) => (
          <a key={item.title} href={`/categories/${item.id}`} className="group">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-3xl bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
              <img
                src={item.image}
                alt={item.title}
                width={400}
                height={400}
                className="h-[400px] w-[400px] object-cover object-center group-hover:opacity-75 "
              />
            </div>
            <h3 className="mt-4 text-lg text-center text-gray-700 font-extrabold">
              {item.title}
            </h3>
            {/* <p className="mt-1 text-lg font-medium text-gray-900">{item.}</p> */}
          </a>
        ))}
      </div>
    </div>
  );
};

export default Categories;
