import React from "react";
import Categories from "../categories/Categories.tsx";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <nav>
      <ul className=" text-sm font-medium text-center text-gray-500 rounded-lg shadow sm:flex dark:divide-gray-700 dark:text-gray-400">
        <li className="w-full">
          <a
            href="/categories"
            className="inline-block w-full p-4 text-gray-900 bg-gray-100 border-r border-gray-200 dark:border-gray-700 rounded-s-lg focus:ring-4 focus:ring-blue-300 active focus:outline-none dark:bg-gray-700 dark:text-white"
            aria-current="page"
          >
            Categories
          </a>
        </li>
        <li className="w-full">
          <a
            href="/orders"
            className="inline-block w-full p-4 bg-white border-r border-gray-200 dark:border-gray-700 hover:text-gray-700 hover:bg-gray-50 focus:ring-4 focus:ring-blue-300 focus:outline-none dark:hover:text-white dark:bg-gray-800 dark:hover:bg-gray-700"
          >
            Orders
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
