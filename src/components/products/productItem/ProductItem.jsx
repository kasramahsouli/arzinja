import React, { useState } from "react";

import { useMutation } from "react-query";
import axios from "axios";
import { useQueryClient } from "react-query";
import Map from "../../map/Map";

const ProductItem = ({ product }) => {
  const [open, setOpen] = useState(false);
  const [order, setOrder] = useState(product);
  const queryClient = useQueryClient();
  const { name, description, price, image } = product;
  const addToOrdersList = async (product) => {
    return axios
      .post("http://localhost:8000/api/v1/orders", product)
      .then((res) => res.data);
  };
  const mutation = useMutation({
    mutationFn: addToOrdersList,
    onSuccess: () => {
      queryClient.invalidateQueries(["orderTracking"], { exact: true });
    },
  });
  const clickHandler = () => {
    setOpen(true);

    // mutation.mutate(product);
  };
  return (
    <div className="">
      {open && (
        <Map
          setOpen={setOpen}
          location={order.addressLatLng}
          onChange={(latlng) => {
            setOrder({ ...order, addressLatLng: latlng });
          }}
        />
      )}
      <a href="#" className="group relative block bg-black">
        <img
          alt="Developer"
          src={image}
          className="absolute inset-0 h-full w-full object-cover opacity-75 transition-opacity group-hover:opacity-50"
        />

        <div className="relative p-4 sm:p-6 lg:p-8">
          <p className="text-sm font-medium uppercase tracking-widest text-pink-500">
            {description}
          </p>
          <p className="text-lg font-bold sm:text-2xl">{price}</p>

          <p className="text-xl font-bold text-white sm:text-2xl">{name}</p>

          <div className="mt-32 sm:mt-48 lg:mt-64">
            <div className="translate-y-8 transform opacity-0 transition-all group-hover:translate-y-0 group-hover:opacity-100">
              <button
                type="button"
                onClick={clickHandler}
                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                <svg
                  class="w-3.5 h-3.5 me-2"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 18 21"
                >
                  <path d="M15 12a1 1 0 0 0 .962-.726l2-7A1 1 0 0 0 17 3H3.77L3.175.745A1 1 0 0 0 2.208 0H1a1 1 0 0 0 0 2h.438l.6 2.255v.019l2 7 .746 2.986A3 3 0 1 0 9 17a2.966 2.966 0 0 0-.184-1h2.368c-.118.32-.18.659-.184 1a3 3 0 1 0 3-3H6.78l-.5-2H15Z" />
                </svg>
                Buy now
              </button>
            </div>
          </div>
        </div>
      </a>
    </div>
  );
};

export default ProductItem;
